package oops;
class A{
	
	public void eat() {
		System.out.println("I am eating in my parent's house");
	}
}


class B extends A{
	
	public void eat() {
		System.out.println("I am eating in my house");
	}
	
	public void run() {
		System.out.println("I am a child and I like running");
	}
}


public class InheritanceDemo2 {

	public static void main(String[] args) {
//		A a = new A();
//		a.eat(); //I am eating in my parent's house
//
//
//		B b = new B();
//		b.run(); // I am a child and I like running
//		b.eat(); //I am eating in my parent's house
		
		A a = new B();
//		a.eat(); // valid
//		a.run(); // Invalid 
//		a.eat(); // I am eating in my house
		
		
		
	}
}
