package oops;

class Parent{
	
	public void method1() {
		System.out.println("method1 in parent class is executing.");
	}
	
}

class Child extends Parent{
	
//	public void method1() {
//		System.out.println("method1 in parent class is executing.");
//	}

}

public class InheritanceDemo{
	
	public static void main(String[] args) {
		
		Parent p =new Parent();
		p.method1(); // method1 in parent class is executing.
		
		Child c = new Child();
		c.method1(); // method1 in parent class is executing.
		
	}
	
}