package learning_java;
class ObjectFlow{

	public static int number = 2;

	int instanceVariable = 10;

	static void method1() {
//		method2();
		System.out.println("This is a static method1...");
	}

	public ObjectFlow() {
		method1();
		System.out.println("This is a no-arg constructor");
	}

	public void method2() {
		method1();
		System.out.println("This is an instance method");
	}
}

public class ObjectFlowDemo {
	public static void main(String[] args) {
//		ObjectFlow objectFlowDemo = new ObjectFlow();
		
		// static invocations
//		System.out.println(ObjectFlow.number); // 2
//		ObjectFlow.method1(); //m1
		
		// instance invocation
		ObjectFlow objectFlow = new ObjectFlow(); // constructor
//		System.out.println(objectFlow.instanceVariable); // instance variables
//		objectFlow.method2(); // instance method
	}
}

//1. initialize static variables
//2. initialize static blocks
//3. load static methods

// related to class

// loaded/initialized during class loading into JVM

//4. call constuctor(no-arg/parameterized)

// object initialization

//5. instance variables
//6. instance blocks
//7. instance methods

// related to object/instance. initialized during object access.

// accessing non-static area to static area is possible, 
// but accessing static area to non-static area is not at all possibe