package learning_java;
public class Test {

	public static void main(String[] args) {
		int age = 10;

		if (age < 13 || age > 19) {
			System.out.println("I am Sorry, You are not a teenager yet.");
		} else {
			System.out.println("Congratulations buddy, you are a teenager now.");
		}
	}

}
