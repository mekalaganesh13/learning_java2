package learning_java;

public class test1 {
	class CheckDiscount {
		public void main(String[] args) {
			int coffees_ordered_in_last_week = 10;
			
			int discount = 0;
			
			String user = "Duplicate Chiranjeevi";
			
			if (coffees_ordered_in_last_week > 5) { // 10>5=true
				discount = 10;
				// 2 == 2 = true
				// "chiranjeevi"=="chiranjeevi" =  true
				if (user.equals("Chiranjeevi")) {
					discount = 50;
				}
				System.out.println("Hello there. You are getting a discount of : "+ discount);
			} else {
				discount = 5;
			}
			System.out.println("This customer is eligible for a " + discount + "% discount.");
		}
	}

}
