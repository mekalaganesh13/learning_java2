package learning_java;

class Car{

	private String color;
	private int mfgYear;
	
	// Car(){
	//}

	public void setColor(String color) {
		this.color = color;
	}

	public String getColor() {
		return this.color;
	}

	public void setMfgYear(int mfgYear) {
		this.mfgYear = mfgYear;
	}

	public int getMfgYear() {
		return this.mfgYear;
	}
}

public class CarDemo {

	public static void main(String[] args) {
		
		Car car = new Car();
		
		car.setColor("Lite maroon");
		car.setMfgYear(2018);
		
		String color = car.getColor();
		int year = car.getMfgYear();
		
	}
}





