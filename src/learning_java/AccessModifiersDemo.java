package learning_java;

class AccessModifiers{
	
	private int number = 10;
	
	void method1() {
		System.out.println("printing method1");
	}
	
	public int getNumber() {
		return this.number;
	}
}

public class AccessModifiersDemo {

	public static void main(String[] args) {
		
		AccessModifiers accessModifiers =  new AccessModifiers();
		System.out.println(accessModifiers.getNumber());;
		accessModifiers.method1();
		System.out.println(accessModifiers.getNumber());;
	}
}
