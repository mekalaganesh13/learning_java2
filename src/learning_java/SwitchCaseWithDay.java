package learning_java;
import java.time.LocalDate;

public class SwitchCaseWithDay {

	public static void main(String[] args) {
		LocalDate today = LocalDate.now();
		int month = today.getMonthValue();

		switch (month) {
		case 1:
			System.out.println("January");
			break;
		case 2:
			System.out.println("February");
			break;
		case 3:
			System.out.println("March");
			break;
		case 4:
			System.out.println("April");
			break;
		case 5:
			System.out.println("May");
			break;
		case 6:
			System.out.println("June");
			break;
		}

	}
}
