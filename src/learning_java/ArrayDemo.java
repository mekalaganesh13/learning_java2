package learning_java;

public class ArrayDemo {

	public static void main(String[] args) {
		
		int[] marks = new int[]{60,48,56,39,75,84,100};
//								0	1  2  3  4  5  6  
		
//					    i<7
		for(int i=0;i<marks.length;i=i+1) {
			if(marks[i]%2==0) {
				System.out.println(marks[i]+" is an even number");
			}else {
				System.out.println(marks[i]+" is an odd number");
			}
		}
		
	}
}
