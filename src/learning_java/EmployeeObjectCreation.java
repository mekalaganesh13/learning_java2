package learning_java;

class Employee {
	
	public void walkToOffice() {
		System.out.println("Employee walking to office");
	}

	public void returnHome() {
		System.out.println("Employee returning from office.");
	}
}


public class EmployeeObjectCreation{
	
	public static void main(String[] args) {
		
		Employee john = new Employee();
		john.walkToOffice();
		john.returnHome();

		Employee krishna  =  new Employee();
		krishna.walkToOffice();
		krishna.returnHome();
		
	}
}